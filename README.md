# JRP (java reverse proxy)
JRP 是一个反向代理项目，可以运行在J2EE engine上。JRP是java开源项目，设计为运行在tomcat上。实行可以很好的运行在任何有java WEB容器上。包含一系列的基础映射规则，也很方面的扩展，以慢足您的功能业务需求。
![处理原理](http://git.oschina.net/uploads/images/2016/1223/151142_9e1e1d02_773390.png "JRP反向代理原理图")
# about j2ep
项目来源于 http://ncu.dl.sourceforge.net/project/j2ep/j2ep/j2ep-1.0/j2ep-1.0-src.zip

J2EP is a reverse proxy running on a J2EE engine. The proxy is written in java and was originally designed with Tomcat in mind, but any engine should work fine. A set of basic mapping rules are included but they can easily be extended to your own needs.
# j2ep in github
github:https://github.com/jacarrichan/j2ep/

#关于java反向代理
参考:
http://datalab.int-yt.com/archives/27
https://wiki.apache.org/tomcat/ServletProxy
http://noodle.tigris.org/