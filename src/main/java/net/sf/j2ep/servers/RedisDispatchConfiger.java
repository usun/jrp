package net.sf.j2ep.servers;

import java.util.HashSet;
import java.util.Set;

import net.sf.j2ep.servers.DispatchConfigerFactory.DispatchConfiger;

/**
 * 
* ClassName: RedisDispatchConfiger
* description: redis分发配置
* date: 2016年12月26日 下午4:19:18
* @author usun
* @version
 */
public class RedisDispatchConfiger implements DispatchConfiger  {
	
	public String getServerId(String dispatchValue){
		String serverId = "mysql";
		if (null == dispatchValue) return serverId;
		if (dispatchValue.equals("user1")){
			serverId = "mysql";
		} else if(dispatchValue.equals("user2")) {
			serverId = "oracle";
		} else if (dispatchValue.equals("user3")){
			serverId = "sqlserver";
		}
		return serverId;
	}
	
}
