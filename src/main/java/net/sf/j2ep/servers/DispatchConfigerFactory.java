package net.sf.j2ep.servers;

public class DispatchConfigerFactory {
	
	/**
	 * 
	 * @description 分发配置工厂
	 * @author usun
	 * @param dispatchCat
	 * @return
	 * @throws Exception
	 */
	public static DispatchConfiger factory(String dispatchCat) throws Exception{
		if (dispatchCat.equalsIgnoreCase("redis")){
			return new RedisDispatchConfiger();
		} else {
			throw new Exception("DispatchCluster server: bad dispatcher request!!!");
		}
		
	}
	
	/**
	 * 
	* ClassName: DispatchConfig
	* description: 分发配置
	* date: 2016年12月26日 下午4:19:18
	* @author usun
	* @version
	 */
	public  interface DispatchConfiger  {
		/**
		 * 
		 * @description 从配置得到用户分发的serverId
		 * @author usun
		 * @param dispatchValue
		 * @return
		 */
		String getServerId(String dispatchValue);
	}
}
