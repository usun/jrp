/*
 * Copyright 2000,2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.j2ep.requesthandlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;


import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.EntityEnclosingMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import net.sf.j2ep.servers.DispatchCluster;

/**
 * Handler for POST and PUT methods.
 *
 * @author Anders Nyman
 */
public class EntityEnclosingRequestHandler extends RequestHandlerBase {

    /**
     * Will set the input stream and the Content-Type header to match this request.
     * Will also set the other headers send in the request.
     * @description 需要注意的是(http://www.cnblogs.com/xiancheng/p/5524338.html)：
request.getParameter()、 request.getInputStream()、request.getReader()这三种方法是有冲突的，因为流只能被读一次。
比如：
当form表单内容采用 enctype=application/x-www-form-urlencoded编码时，先通过调用request.getParameter()方法得到参数后,
再调用request.getInputStream()或request.getReader()已经得不到流中的内容，
因为在调用 request.getParameter()时系统可能对表单中提交的数据以流的形式读了一次,反之亦然。

当form表单内容采用 enctype=multipart/form-data编码时，即使先调用request.getParameter()也得不到数据，
所以这时调用request.getParameter()方法对 request.getInputStream()或request.getReader()没有冲突，
即使已经调用了 request.getParameter()方法也可以通过调用request.getInputStream()或request.getReader()得到表单中的数据,
而request.getInputStream()和request.getReader()在同一个响应中是不能混合使用的,如果混合使用就会抛异常。
     * @throws IOException An exception is throws when there is a problem getting the input stream
     * @see net.sf.j2ep.model.RequestHandler#process(javax.servlet.http.HttpServletRequest, java.lang.String)
     */
    public HttpMethod process(HttpServletRequest request, String url) throws IOException {
        
        EntityEnclosingMethod method = null;
        
        if (request.getMethod().equalsIgnoreCase("POST")) {
            method = new PostMethod(url);
        } else if (request.getMethod().equalsIgnoreCase("PUT")) {
            method = new PutMethod(url);
        }
        
        setHeaders(method, request);
        InputStreamRequestEntity stream = null;
        //method.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");
        if (null != request.getAttribute(DispatchCluster.REQUEST_STREAM_STRING) && request.getAttribute(DispatchCluster.REQUEST_STREAM_STRING).toString().trim().length() > 0) {
        	stream = new InputStreamRequestEntity(new ByteArrayInputStream(request.getAttribute(DispatchCluster.REQUEST_STREAM_STRING).toString().getBytes()));
        } else {
        	stream = new InputStreamRequestEntity(request.getInputStream());
        }
        method.setRequestEntity(stream);
        //method.setRequestHeader("Content-type", request.getContentType());
        method.addRequestHeader("Content-type", request.getContentType());
        return method;
        
    }
        

}
